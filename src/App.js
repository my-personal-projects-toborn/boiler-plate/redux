import "./App.css";

import Layout from "./layout";
import Pages from "./pages";

function App() {
  return (
    <Layout>
      <Pages></Pages>
    </Layout>
  );
}

export default App;
